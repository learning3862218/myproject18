﻿#include <iostream>
#include <string>

class Player
{
private:
    std::string _Name;
    int _Score;

public:
    Player() : _Name("No Name"), _Score(0) {};
    Player(std::string Name) : _Name(Name), _Score(0) {};
    Player(std::string Name, int Score): _Name(Name), _Score(Score) {};

    void SetName(std::string Name)
    {
        if (!Name.empty())
        {
            _Name = Name;
        }
    }

    std::string GetName()
    {
        return _Name;
    }

    void SetScore(int Score)
    {
        if (Score >= 0)
        {
            _Score = Score;
        }
    }

    int GetScore()
    {
        return _Score;
    }
};

// ввод и проверка количества игроков
void InputPlayersCount(int& PlayersCount)
{
    do {
        std::cout << "Enter number of players: ";
        std::cin >> PlayersCount;
        if (!std::cin.good() || PlayersCount < 1)
        {
            std::cout << "Wrong number!" << std::endl;
            std::cin.clear();
            std::cin.ignore();
        }
    } while (PlayersCount < 1);
}

// заполнение массива игроков (ввод имен и счета каждого игрока)
void InputPlayers(Player *Players, const int PlayersCount)
{
    std::cin.ignore();
    for (int i = 0; i < PlayersCount; ++i)
    {
        Player TempPlayer;
        std::string Name{};
        int Score{};
        std::cout << "Enter player #" << i + 1 << " name: ";
        std::getline(std::cin, Name);
        do {
            std::cout << "Enter player #" << i + 1 << " score: ";
            std::cin >> Score;
            if (!std::cin.good() || Score < 0)
            {
                std::cout << "Wrong score!" << std::endl;
                std::cin.clear();
                std::cin.ignore();
            }
        } while (Score < 1);
        std::cin.ignore();
        TempPlayer.SetName(Name);
        TempPlayer.SetScore(Score);
        *(Players + i) = TempPlayer;
    }
}

// сортировка массива от максимального счёта к минимальному
void SortPlayers(Player* Players, const int PlayersCount)
{
    int Offset = 0;
    while (Offset < PlayersCount / 2)
    {
        int MaxScore = Players[Offset].GetScore();
        int MaxIndex = Offset;
        int RightIndex = PlayersCount - Offset - 1;
        int MinScore = Players[RightIndex].GetScore();
        int MinIndex = PlayersCount - Offset - 1;
        for (int i = 0 + Offset; i < PlayersCount - Offset; ++i)
        {
            if (Players[i].GetScore() < MinScore)
            {
                MinScore = Players[i].GetScore();
                MinIndex = i;
            }
            if (Players[i].GetScore() > MaxScore)
            {
                MaxScore = Players[i].GetScore();
                MaxIndex = i;
            }
        }
        Player TempPlayer;
        if (MinIndex == Offset && MaxIndex == RightIndex)
        {
            std::swap(Players[Offset], Players[RightIndex]);
        } 
        else if (MaxIndex == RightIndex)
        {
            std::swap(Players[Offset], Players[MaxIndex]);
			if (Offset != PlayersCount / 2)
			{
                std::swap(Players[RightIndex], Players[MinIndex]);
			}
        }
        else {
            std::swap(Players[RightIndex], Players[MinIndex]);
			if (Offset != PlayersCount / 2)
			{
                std::swap(Players[Offset], Players[MaxIndex]);
			}
        }
        ++Offset;
    }
}


int main()
{
    int PlayersCount{};
    InputPlayersCount(PlayersCount);

    Player* Players = new Player[PlayersCount];
    InputPlayers(Players, PlayersCount);

    SortPlayers(Players, PlayersCount);

    // вывод отсортированного списка игроков
    for (int i = 0; i < PlayersCount; ++i)
    {
        if ((Players + i) != nullptr)
        {
            std::cout << (Players + i)->GetName() << "\t" << (Players + i)->GetScore() << std::endl;
        }
    }

    delete[] Players;
}
